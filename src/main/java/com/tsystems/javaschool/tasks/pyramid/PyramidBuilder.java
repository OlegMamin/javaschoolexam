package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        //determining the number of rows in pyramid if it's possible. CannotBuildPyramidException may be thrown
        int numOfRows = getNumOfRows(inputNumbers);

        //sorting numbers in inputNumbers
        List<Integer> sortedList = new ArrayList<>(inputNumbers);
        Collections.sort(sortedList);

        // filling pyramid with digits
        return fillingWithDigits(numOfRows, sortedList, 0);
    }

    private int[][] fillingWithDigits(int numOfRows, List<Integer> inputNumbers, int defaultCell){
        int[][] result = new int[numOfRows][numOfRows * 2 - 1];

        int center = numOfRows - 1;
        Iterator<Integer> iterator = inputNumbers.iterator();
        for (int i = 0; i < result.length; i++) {
            Arrays.fill(result[i], defaultCell);
            //filling array rows with inputNumber elements
            for (int k = 0; k <= i; k++) {
                if (iterator.hasNext()) {
                    result[i][center - i + k * 2] = iterator.next();
                }
            }
        }
        return result;
    }

    private int getNumOfRows(List<Integer> inputNumbers){
        int numOfRows = 0;
        int size = inputNumbers.size();
        for (int i = 1; i <= inputNumbers.size(); i++) {
            size -= i;
            if (size > 0) {
                continue;
            } else if (size == 0) {
                numOfRows = i;
                break;
            } else {
                throw new CannotBuildPyramidException();
            }
        }
        return numOfRows;
    }
}
