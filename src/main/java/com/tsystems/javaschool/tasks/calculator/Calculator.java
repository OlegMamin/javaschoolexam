package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Locale;
import java.util.StringTokenizer;

public class Calculator {
    // available operators
    private static final String OPERATORS = "+-*/";

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // stack for holding expression converted to reversed polish notation
        try {
            Deque<String> stackRPN = parseToRPN(statement);

            double resultRPN = calculateRPN(stackRPN);

            // rounding and formatting resultRPN
            NumberFormat nf = NumberFormat.getInstance(Locale.US);
            nf.setMaximumFractionDigits(4);

            return nf.format(resultRPN);
        } catch (Exception e) {
            return null;
        }
    }

    // calculating result of Reverse Polish Notation
    private double calculateRPN(Deque<String> stackRPN){

        // stack for holding the calculations result
        Deque<Double> stackAnswer = new LinkedList<>();

        String token;
        double result = 0;
        double d1;
        double d2;
        while (!stackRPN.isEmpty()){
            token = stackRPN.removeFirst();
            if (!isOperator(token)) {
                result = Double.parseDouble(token);
            } else {
                d1 = stackAnswer.removeLast();
                d2 = stackAnswer.removeLast();
                switch (token){
                    case "+":
                        result = d2 + d1;
                        break;
                    case "-":
                        result = d2 - d1;
                        break;
                    case "*":
                        result = d2 * d1;
                        break;
                    case "/":
                        if (d1 == 0d) throw new ArithmeticException();
                        result = d2 / d1;
                        break;
                }
            }
            stackAnswer.add(result);
        }
        return stackAnswer.removeLast();
    }

    // parsing statement to Reverse Polish Notation
    private Deque<String> parseToRPN(String statement){
        // stack for holding expression converted to reversed polish notation
        Deque<String> tempSPN = new LinkedList<>();

        // stack that holds operators and brackets
        Deque<String> stackOperations = new LinkedList<>();

        // preparing statement: replacing spaces and make unary minus possible to evaluate
        statement = statement.replace(" ", "").replace("(-", "(0-");
        if (statement.charAt(0) == '-') {
            statement = "0" + statement;
        }

        // taking tokens from statement
        StringTokenizer stringTokenizer = new StringTokenizer(statement,
                OPERATORS + "()", true);

        // parsing to Reverse Polish Notation using shunting-yard algorithm
        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (isNumber(token)) {
                tempSPN.add(token);
            } else if (isOperator(token)){
                String firstInOperations = stackOperations.peekFirst();
                while (!stackOperations.isEmpty() && isOperator(firstInOperations) &&
                        getPrecedence(token) <= getPrecedence(firstInOperations)){
                    tempSPN.add(stackOperations.removeFirst());
                    firstInOperations = stackOperations.peekFirst();
                }
                stackOperations.push(token);
            } else if (isOpenBracket(token)){
                stackOperations.push(token);
            } else if (isCloseBracket(token)){
                while (!stackOperations.isEmpty() && !isOpenBracket(stackOperations.peekFirst())){
                    tempSPN.add(stackOperations.removeFirst());
                }
                stackOperations.removeFirst();
            }
        }
        //adding rest of operators from stackOperations
        while (!stackOperations.isEmpty()){
            tempSPN.add(stackOperations.removeFirst());
        }
        return tempSPN;
    }

    // helper methods for work with tokens
    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
    private boolean isOpenBracket(String token) {
        return "(".equals(token);
    }
    private boolean isCloseBracket(String token) {
        return ")".equals(token);
    }
    private boolean isOperator(String token) {
        return OPERATORS.contains(token);
    }
    private int getPrecedence(String token) {
        if ("+".equals(token) || "-".equals(token)) {
            return 1;
        }
        if ("*".equals(token) || "/".equals(token)) {
            return 2;
        }
        return 0;
    }
}
