package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null){
            throw new IllegalArgumentException();
        }
        int xSize = x.size();
        int ySize = y.size();
        int j = 0;
        // Traverse x and y, and compare current element of y with first
        // unmatched element of x, if matched then move ahead in x
        for (int i = 0; i < ySize && j < xSize; i++) {
            if (x.get(j).equals(y.get(i))) {
                j++;
            }
        }
        // If all elements of x were found in y
        return (j == xSize);
    }
}
